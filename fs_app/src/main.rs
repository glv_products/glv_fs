use clap::Parser;
use fs_shared::get_size_folder;

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct UserInput {
  // GLV - 2023-11-05 - Ругается на ненужные скобки, 
  // но если я их уберу то не соберется... Треш парсер 
  // или недоработка clap библиотеки...
  #[arg(short, long, default_value_t = (".".to_string()))]
  path: String
}

fn main() {
    let args = UserInput::parse();
    let res = get_size_folder(args.path.as_str());
    match res {
      Ok(filesize) => {
        println!("The '{}' folder size is - {}", args.path, filesize);
      },
      Err(error) => {
        println!("Error: {}", error);
      }
    }
}
