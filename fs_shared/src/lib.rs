#![allow(unused)]
#![allow(dead_code)]
use std::path::Path;
use std::io;
use std::fs;
use std::fs::metadata;

pub fn get_size_file<P: AsRef<Path>>(path: P) -> Option<usize> {
  let filesize: Option<usize> = match metadata(&path) {
    Ok(meta) => Some(meta.len() as usize),	
    Err(_) => None
  };
  filesize
}

pub fn get_size_folder<P: AsRef<Path>>(path: P) -> io::Result<usize> {
    let mut summary_bytesize = 0;
    if path.as_ref().is_dir() {
        for entry in fs::read_dir(path.as_ref())? {
            let entry = entry?;
            let path = entry.path();
            if path.is_dir() {
                summary_bytesize += get_size_folder(&path)?;
            } else {
                let posible_filesize = get_size_file(&path);
                if let Some(filesize) = posible_filesize {
                    summary_bytesize += filesize;
                }
            }
        }
    }
    Ok(summary_bytesize)
}

#[cfg(test)]
mod tests {
  use super::*;
}


