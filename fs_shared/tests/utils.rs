use std::fmt::Display;
use std::path::Path;
use std::fs::{File, self};
use std::io::prelude::*;
use std::io;

pub fn create_fake_file<P: AsRef<Path>>(path: P, bytesize: usize) -> io::Result<()> {
  let mut file = File::create(path)?;
  let buffer = vec![0xEF; bytesize];
  file.write_all(buffer.as_slice())?;
  Ok(())
}

pub fn delete_fake_file<P: AsRef<Path> + Display>(path: P) {
  std::fs::remove_file(&path)
    .expect(format!("Cant delete file at {}", &path).as_str());
}

// TODO: GLV - 2023-11-05 - Разобраться почему 
// тут нельзя AsRef<Path>.
pub fn file_not_exists(path: &str) -> bool {
  !std::path::Path::new(path).exists()
}

pub fn create_fake_folder<P: AsRef<Path>>(path: P) {
  match std::fs::create_dir(&path) {
    Ok(_res) => {},
    Err(_err) => {}
  }
}

pub fn delete_fake_folder<P: AsRef<Path>>(path: P) {
  match fs::remove_dir(path) {
    Ok(_res) => {},
    Err(_err) => {}
  }
}