#![allow(unused)]
#![allow(dead_code)]

mod utils;

use fs_shared::{
  get_size_folder, 
  get_size_file
};
use utils::{
  create_fake_file, 
  delete_fake_file,
  create_fake_folder,
  delete_fake_folder,
  file_not_exists,
};

#[test]
fn file_size() {
  let path = "test.bin";
  let expected_bytesize: usize = 12312;
  create_fake_file(&path, expected_bytesize);
  let filesize = get_size_file(&path).unwrap();
  assert_eq!(filesize, expected_bytesize);
  delete_fake_file(&path);
}

#[test]
fn folder_size() {
  let folderpath: &str = "fake";
  create_fake_folder(&folderpath);
  create_fake_file("fake\\fake1.bin", 12);
  create_fake_file("fake\\fake2.bin", 32);
  create_fake_file("fake\\fake3.bin", 123);
  let expected_byte_size = 12 + 32 + 123;
  let r = get_size_folder(folderpath);
  assert_eq!(expected_byte_size, r.unwrap());
  delete_fake_file("fake\\fake1.bin");
  delete_fake_file("fake\\fake2.bin");
  delete_fake_file("fake\\fake3.bin");
  delete_fake_folder("fake");
}

#[test]
fn check_filesize_of_non_existant_file() {
  let filepath = "miracle.txt"; 
  let fsr = get_size_file(&filepath);
  assert!(file_not_exists(&filepath));
  assert!(fsr.is_none());
}